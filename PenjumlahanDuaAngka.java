package penjumlahanduaangka;
import java.util.*;
public class PenjumlahanDuaAngka {

    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        int a,b,hasil;
        System.out.println("Penjumlahan Dua Angka");
        System.out.println("=====================\n");
       
     
        System.out.print("Masukkan angka pertama  : ");
        a=input.nextInt();
        System.out.print("Masukkan angka kedua    : ");
        b=input.nextInt();
        hasil=a+b;
        
        System.out.println("\nHasil penjumlahan       : "+hasil);
    }  
}
