package characterizedstring;
import java.util.Scanner;
public class CharacterizedString {
    public static void main(String[] args) {
        Scanner input=new Scanner(System.in);
        String kata;
        
        System.out.print("Ketik kata/kalimat, lalu tekan enter : ");
        kata=input.nextLine();
        
        System.out.println("=======================================");
        System.out.println("String penuh: "+ kata);
        System.out.println("=======================================");
        System.out.println("Process memecahkan kata/kalimat menjadi karakter....");
        System.out.println("Done.");
        System.out.println("=======================================");
  
        int jumlahKarakter=kata.length();
        for(int i=1;i<=jumlahKarakter;i++){
         System.out.println("Karakter["+i+"]    : "+kata.charAt(i-1));   
       }
        System.out.println("========================================");
    } 
}
